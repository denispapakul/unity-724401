﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    new AudioSource audio;
    public AudioClip buttonSound;
    public AudioClip lowTime;
    public AudioClip fail;
    public AudioClip accept;
    public AudioClip pick;

    public bool isPlaying;

    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    public void ButtonSound()
    {
        audio.PlayOneShot(buttonSound);
    }

    public void LowTime()
    {
        audio.PlayOneShot(lowTime);
    }

    public void Fail()
    {
        audio.PlayOneShot(fail);
    }
    
    public void Accept()
    {
        audio.PlayOneShot(accept);
    }

    public void Pick()
    {
        audio.PlayOneShot(pick);
    }
}
