﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    public TMP_Text highScore;
    private void Start()
    {
       // DontDestroyOnLoad(this.gameObject);
        highScore.text = "Highscore: " + PlayerPrefs.GetInt("Highscore", 0);
    }
    public void StartGames()
    {
        SceneManager.LoadScene(1);
    }
}
