﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class Card : ScriptableObject
{
    public int cardID;
    public string cardName;
    public CardSprite sprite;
    public string cardYear;
    public string cardSex;
    public string leftDialogue;
    public string rightDialogue;
    public string midDialogue;
}
