﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    public Sprite[] spritesCards;
    public Card[] cards;
    public Sprite[] spriteCharacters;
    public GameObject[] hearts;
    public Sprite[] backGrounds;
}
