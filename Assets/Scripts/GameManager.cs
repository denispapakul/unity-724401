﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
[CreateAssetMenu]
public class GameManager : MonoBehaviour
{
    new AudioSource audio;
    //GameObjects
    public GameObject cardGameObject;
    public SpriteRenderer cardSpriteRender;
    public SpriteRenderer characterSpriteRender;
    public ResourceManager resourceManager;
    public CardController mainCardController;
    public CharacterManager mainCharacterManager;
    public GameObject GameOverUI;
    public GameObject character;
    //Tweaking variables
    public float fMovingSpeed;
    public int characterIndex;
    public int cardIndex;
    Vector3 pos;
    public static int counter;
    //UI
    public TMP_Text characterYear;
    public TMP_Text characterSex;
    public TMP_Text characterName;
    public TMP_Text sucsessCounter;
    public TMP_Text highScore;
    public TMP_Text midDialogue;
    //Card variables
    private string cardSex;
    public Card currentCard;
    public GameObject WinWindow;

    int randomYear;
    int rollDice;
    int probability;
    int randomSex;
    int hearth = 3;

    int conditionValue;
    delegate void Checker();
    Checker checker;
    bool isCorrect;

    public Image backGround;
    public TMP_Text conditionText;
    public GameObject MenuUI;
    public GameObject storyUI;
    public GameObject satanUI;
    public GameObject drunkUI;
    public GameObject winGameUI;
    public static bool restartGame;
    public int quequeValue;

    public int levelCounter;

    public GameObject timer;
    int playSound = 0;
    Vector3 spawnPos = new Vector3(-0.22f, 0.06f, 0.0f);

    Queue<GameObject> characterQueue = new Queue<GameObject>();


    public TMP_Text timerText;
    public int timeLeft = 10;
    GameManager gameManager;
    float gameTime;

    AudioManager audioManager;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        if (restartGame)
        {
            MenuUI.SetActive(false);
            timer.SetActive(true);
            StartCoroutine(TimerT());
        }
        else
        {
            MenuUI.SetActive(true);
            timer.SetActive(false);
        }
        audio.volume = 1;
        highScore.text = "Highscore: " + PlayerPrefs.GetInt("Highscore", 0);
        counter = 0;
        int randomBack = Random.Range(0, 17);
        backGround.sprite = resourceManager.backGrounds[randomBack];
        conditionValue = Random.Range(1, 8);
        switch (conditionValue)
        {
            case 1:
                checker += CheckPhoto;
                conditionText.text = "1)Match Foto!";
                break;
            case 2:
                checker += ChechSex;
                conditionText.text = "1)Match Sex!";
                break;
            case 3:
                checker += CheckYear;
                conditionText.text = "1)18 years or more!";
                break;
            case 4:
                checker += CheckPhoto;
                checker += ChechSex;
                conditionText.text = "1)Mathc Foto!\n\n2)Match Sex!";
                break;
            case 5:
                checker += CheckPhoto;
                checker += CheckYear;
                conditionText.text = "1)Mathc Foto!\n\n2)18 years or more!";
                break;
            case 6:
                checker += CheckYear;
                checker += ChechSex;
                conditionText.text = "1)18 years or more!\n\n2)Match Sex!";
                break;
            case 7:
                checker += CheckPhoto;
                checker += ChechSex;
                checker += CheckYear;
                conditionText.text = "1)Mathc Foto!\n\n2)Match Sex!\n\n3)18 years or more";
                break;
        }


        spawnPos = character.transform.position;
        for (quequeValue = 0; quequeValue < Random.Range(10,36); quequeValue++)
        {
            //LoadCard(resourceManager.cards[rollDice]);
            NewCharacter(rollDice);
            var go = Instantiate(character, spawnPos, Quaternion.identity);
            characterQueue.Enqueue(go);
            spawnPos.x += -2f;
            
        }
        characterIndex = characterQueue.Peek().GetComponent<CharacterManager>().index;
        LoadCard(resourceManager.cards[characterQueue.Peek().GetComponent<CharacterManager>().index]);
        cardIndex = currentCard.cardID;
        GameOverUI.SetActive(false);
        timeLeft = quequeValue * 5;
    }

    void UpdateDialogue(bool correct)
    {
        if (correct)
        {
            counter++;
            audioManager.Accept(); 
            sucsessCounter.text = counter.ToString("0##");
        }
        else 
        {
            audioManager.Fail();
            hearth--;
            resourceManager.hearts[hearth].SetActive(false);
            if (counter > 0)
            {
                counter--;
                sucsessCounter.text = counter.ToString("0##");
            }
           
           
        }
       
    }

    void Update()
    {
        if (cardGameObject.transform.position.y > -0.69f && cardGameObject.transform.position.y < 1.5f)
        {
            if (Input.GetMouseButton(0))
            {
                midDialogue.text = currentCard.leftDialogue;
            }
            if (Input.GetMouseButtonUp(0))
            {
                Decline();
            }
        }
        else
        {
            if (Input.GetMouseButton(0))
                midDialogue.text = currentCard.midDialogue;
        }

        if (Input.GetMouseButtonDown(0) && mainCardController.isMouseOver)
        {
            audioManager.Pick();
        }

            //Movement
            if (Input.GetMouseButton(0) && mainCardController.isMouseOver)
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);           
            cardGameObject.transform.position = pos;
        }
        else
        {
            cardGameObject.transform.position = Vector2.MoveTowards(cardGameObject.transform.position, new Vector2(-2.04f, -2.58f), fMovingSpeed);
        }     

        if(hearth == 0)
        {         
            audio.volume = 0;
            GameOverUI.SetActive(true);
            timer.SetActive(false);
            StopAllCoroutines();
        }

        if (characterQueue.Count == 0)
        {
            audio.volume = 0;
            WinWindow.SetActive(true);
            timer.SetActive(false);
            StopAllCoroutines();
        }

        if (timeLeft < 20)
        {          
            timerText.color = Color.red;
        }

        if(timeLeft == 0)
        {
           
            audio.volume = 0;
            GameOverUI.SetActive(true);
            timer.SetActive(false);
            StopAllCoroutines();
        }
        //characterIndex = characterQueue.Peek().GetComponent<CharacterManager>().index;

        //if(character.transform.position.x <= -0.22f)
        //{
        //    character.transform.position = Vector3.MoveTowards(character.transform.position, new Vector3(-0.22f, 0.06f, 0f), Time.deltaTime);
        //}

        if(levelCounter == 5)
        {
            if (PlayerPrefs.GetInt("Highscore", 0) < counter)
            {
                PlayerPrefs.SetInt("Highscore", counter);
            }          
            audio.volume = 0;
            timer.SetActive(false);
            StopAllCoroutines();
            winGameUI.SetActive(true);
        }

    }

    public IEnumerator TimerT()
    {
        timerText.text = timeLeft + " sec left";
        while (true)
        {
            yield return new WaitForSecondsRealtime(1);
            timerText.text = timeLeft + " sec left";
            timeLeft--;          
        }

    }


    public void LoadCard(Card card)
    {
        cardSpriteRender.sprite = resourceManager.spritesCards[(int)card.sprite];
        currentCard = card;
        //NEW
        randomYear = Random.Range(14, 60);
        card.cardYear = randomYear.ToString();
        randomSex = Random.Range(0, 2);

        if(randomSex == 0)
        {
            card.cardSex = "Male";
        }
        else
        {
            card.cardSex = "Female";
        }
        //NEW
        characterYear.text = card.cardYear + " years";
        characterName.text = card.cardName;
        characterSex.text = card.cardSex;
        midDialogue.text = card.midDialogue; 
    }


    public void NewCard(int k)
    {
        probability = Random.Range(0, 101);
        int m = currentCard.cardID;
        rollDice = k;
        if (probability < 41)
        {
            while (rollDice == m)
            {
                rollDice = Random.Range(0, resourceManager.cards.Length);
            }
            cardIndex = rollDice;
            LoadCard(resourceManager.cards[rollDice]);
        }
        else
        {
            LoadCard(resourceManager.cards[characterIndex]);
        }
       
       
        //if (probability > 40)
        //{
        //    NewCharacter(rollDice);
        //}
        //else
        //{
        //    NewCharacter(Random.Range(0, resourceManager.spriteCharacters.Length)); 
        //}
       
        
    }

    public void NewCharacter(int n)
    {
        int m = characterIndex;
        rollDice = n;
        while (rollDice == m)
        {
            rollDice = Random.Range(0, resourceManager.spriteCharacters.Length);
        }
        characterIndex = rollDice;
        characterSpriteRender.sprite = resourceManager.spriteCharacters[characterIndex];
        //character.transform.position = new Vector3(-4f, 0.06f, 0f);
        character.GetComponent<CharacterManager>().index = characterIndex;
    }

    public void Accept()
    {


        isCorrect = true;
        checker();
        foreach (var go in characterQueue)
        {
            go.transform.position += new Vector3(2f,0,0);
        }

        int k = currentCard.cardID;
        if (isCorrect)
        {
            if(characterQueue.Peek().GetComponent<CharacterManager>().index == 7)
            {
                resourceManager.hearts[0].SetActive(false);
                resourceManager.hearts[1].SetActive(false);
                resourceManager.hearts[2].SetActive(false);
                satanUI.SetActive(true);             
                timer.SetActive(false);
                StopAllCoroutines();
                audio.volume = 0;

            }
            else if (characterQueue.Peek().GetComponent<CharacterManager>().index == 12)
            {
                resourceManager.hearts[0].SetActive(false);
                resourceManager.hearts[1].SetActive(false);
                resourceManager.hearts[2].SetActive(false);
                drunkUI.SetActive(true);
                timer.SetActive(false);
                StopAllCoroutines();               
                audio.volume = 0;
            }
            else
            {
                Debug.Log("You Correct!");
                UpdateDialogue(true);
            }
                   
        }
        else
        {
            if (characterQueue.Peek().GetComponent<CharacterManager>().index == 7)
            {

            }
            else if (characterQueue.Peek().GetComponent<CharacterManager>().index == 12)
            {

            }
            else
            {
                Debug.Log("You Fail!");

                UpdateDialogue(false);
            }
            
        }
       
        Destroy(characterQueue.Peek());
        characterQueue.Dequeue();
        characterIndex = characterQueue.Peek().GetComponent<CharacterManager>().index;
        
        NewCard(k);
        cardIndex = currentCard.cardID;
    }

    public void Decline()
    {

        isCorrect = true;
        checker();
        foreach (var go in characterQueue)
        {
            go.transform.position += new Vector3(2f, 0, 0);
        }

        int k = currentCard.cardID;
        if (isCorrect)
        {
            if (characterQueue.Peek().GetComponent<CharacterManager>().index == 7)
            {
        
            }
            else if (characterQueue.Peek().GetComponent<CharacterManager>().index == 12)
            {
       
            }
            else
            {
                Debug.Log("You Fail!");
                UpdateDialogue(false);
            }
           
           
        }
        else
        {
           Debug.Log("You Correct!");
        }

       
        Destroy(characterQueue.Peek());
        characterQueue.Dequeue();
        characterIndex = characterQueue.Peek().GetComponent<CharacterManager>().index;
       
        NewCard(k);
        cardIndex = currentCard.cardID;

    }

    public void CheckPhoto()
    {
        if (characterIndex == cardIndex)
        {

        }
        else
        {
            Debug.Log("Wrong foto!");
            isCorrect = false;
        }
    }

    public void ChechSex()
    {
        if ((characterIndex == 0 || characterIndex == 4 || characterIndex == 9  || characterIndex == 14) && randomSex == 1)
        {
           
        }
        else if ((characterIndex == 1 || characterIndex == 2 || characterIndex == 3 || characterIndex == 5 || characterIndex == 10 || characterIndex == 6 || characterIndex == 7 || characterIndex == 8 || characterIndex == 11 || characterIndex == 12 || characterIndex == 13) && randomSex == 0)
        {

        }
        else
        {
            Debug.Log("Wrong Sex!");
            isCorrect = false;
        }
    }

    public void CheckYear()
    {
        if(randomYear >= 18)
        {

        }
        else
        {
            Debug.Log("Wrong year!");
            isCorrect = false;
        }
    }

    public void RestartGame()
    {
        restartGame = true;
        audioManager.ButtonSound();
        SceneManager.LoadScene(0);
    }

    public void NextLevel()
    {
        audioManager.ButtonSound();
        levelCounter++;
        timer.SetActive(true);
        if (PlayerPrefs.GetInt("Highscore", 0) < counter)
        {
            PlayerPrefs.SetInt("Highscore", counter);
        }
        if (hearth == 0)
        {
            hearth++;
            resourceManager.hearts[hearth].SetActive(true);
        }

        int randomBack = Random.Range(0, 17);
        backGround.sprite = resourceManager.backGrounds[randomBack];
        conditionValue = Random.Range(1, 8);
        switch (conditionValue)
        {
            case 1:
                checker += CheckPhoto;
                conditionText.text = "1)Match Foto!";
                break;
            case 2:
                checker += ChechSex;
                conditionText.text = "1)Match Sex!";
                break;
            case 3:
                checker += CheckYear;
                conditionText.text = "1)18 years or more!";
                break;
            case 4:
                checker += CheckPhoto;
                checker += ChechSex;
                conditionText.text = "1)Mathc Foto!\n\n2)Match Sex!";
                break;
            case 5:
                checker += CheckPhoto;
                checker += CheckYear;
                conditionText.text = "1)Mathc Foto!\n\n2)18 years or more!";
                break;
            case 6:
                checker += CheckYear;
                checker += ChechSex;
                conditionText.text = "1)18 years or more!\n\n2)Match Sex!";
                break;
            case 7:
                checker += CheckPhoto;
                checker += ChechSex;
                checker += CheckYear;
                conditionText.text = "1)Mathc Foto!\n\n2)Match Sex!\n\n3)18 years or more";
                break;
        }


        spawnPos = character.transform.position;
        for (quequeValue = 0; quequeValue < Random.Range(10, 36); quequeValue++)
        {
            //LoadCard(resourceManager.cards[rollDice]);
            NewCharacter(rollDice);
            var go = Instantiate(character, spawnPos, Quaternion.identity);
            characterQueue.Enqueue(go);
            spawnPos.x += -2f;

        }
        characterIndex = characterQueue.Peek().GetComponent<CharacterManager>().index;
        LoadCard(resourceManager.cards[characterQueue.Peek().GetComponent<CharacterManager>().index]);
        cardIndex = currentCard.cardID;
        GameOverUI.SetActive(false);
        WinWindow.SetActive(false);
        StartCoroutine(TimerT());
        timeLeft = quequeValue * 5;
    }

    public void LoadStory()
    {
        audioManager.ButtonSound();
        storyUI.SetActive(true);
        MenuUI.SetActive(false);
    }

    public void StartGame()
    {
        audioManager.ButtonSound();
        levelCounter = 0;
        audio.volume = 1;
        storyUI.SetActive(false);
        MenuUI.SetActive(false);
        timer.SetActive(true);
        StartCoroutine(TimerT()); 
    }
    public void ReturnToMainMenu()
    {
        audioManager.ButtonSound();
        restartGame = false;
        SceneManager.LoadScene(0);
    }

}
