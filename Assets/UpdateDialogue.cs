﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UpdateDialogue : MonoBehaviour
{
    public TMP_Text midDialogue;
    Card cardScript;
    GameManager gameManager;

    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>(); 
    }

    private void OnMouseEnter()
    {
        midDialogue.text = gameManager.currentCard.rightDialogue;
    }

    private void OnMouseExit()
    {
        midDialogue.text = gameManager.currentCard.midDialogue;
    }
}
